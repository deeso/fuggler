import datetime, sys, argparse, time, logging

from  fuggler.acquisition.cuckoovirtualboxdump import CuckooVboxDump
from fuggler.acquisition.sftpsnapshots import  SFTPSnapshots




'''
example usage
python run_vbdump_move.py /sandbox/test_progs/simplewebping.exe \
                                winxpsp3_analysis \
                                /sandbox/code/git/cuckoo \
                               /research_data/snapshot/tests/

'''


def main( cuckoo_location, machine, malware_file, savelocation, 
          runtime, sleep_time, init_wait, vbox_location, host='127.0.0.1',
          port=22, user=None, password=None, dst_dir=''):
    try:
        start_time = datetime.datetime.now()
        sftp = None
        callback = None

        if not user is None and not password is None:
            sftp = SFTPSnapshots(  user, password, local_base_path=savelocation,
                host=host, port=port,  dst_base_path=dst_dir,
                max_threads = 10)
            callback = sftp.add_file

        cuckoo_vbdump = CuckooVboxDump(malware_file, cuckoo_location, machine, savelocation,
            runtime, sleep_time, init_wait, vbox_location, dump_callback = callback,
            allow_load_time = 5, package='exe', priority=99)

        cuckoo_vbdump.start()

        cnt = 0
        while (not cuckoo_vbdump.processing_complete()):
            if cnt == 0:
                sys.stdout.write ("\n%d ticks have past, waiting on run to complete "%(cuckoo_vbdump.get_ticks()))
            else:
                sys.stdout.write('. ')
            cnt = (cnt + 1) %80
        

        if sftp is None:
            return


        print ("Waiting for transfers to conclude.\n")
        cnt = 0
        while sftp.pending_transfers() > 0:
            if cnt == 0:
                sys.stdout.write ("\n%d pending transfers "%(sftp.pending_transfers()))
            else:
                sys.stdout.write('. ')

            time.sleep(1)
            cnt = (cnt + 1) % 80

        end_time = datetime.datetime.now()
        print ("SFTP Transfers completed after after %d seconds."%(end_time - start_time).seconds)
    except:
        if not sftp is None:
           sftp.stop()
        if not cuckoo_vbdump is None:
           cuckoo_vbdump.stop()
        raise
        




parser = argparse.ArgumentParser()

parser.add_argument("malware_file", type=str, 
                  help="/path/to/the/malware.file ", metavar="PATH_TO_MALWARE")

parser.add_argument("machine", type=str, 
                  help="machine to submit the malware too", metavar="MACHINE")

parser.add_argument("cuckoolocation", type=str,
                  help="location of cuckoo", metavar="CUCKOO")
                  
parser.add_argument("savelocation", type=str,
                  help="location where to save memory images", 
                  metavar="SAVELOCATION")

                  
parser.add_argument("-v", "--vboxlocation", type=str,
                  default='/usr/bin/vboxmanage', help="/path/to/vboxmanage is located", 
                  metavar="VBOXLOCATION")


parser.add_argument("-r", "--runtime", type=int,
                  default=3600, help="number of seconds to run for (3600 == 1 hour)",
                  metavar="RUNTIME")

parser.add_argument("-i", "--initwait", type=int,
                  default=30, help="time to wait before starting the dump of the virtual machines memory", 
                  metavar="INITTIME")

parser.add_argument("-t", "--timing", dest="timing", type=int,
                  default=30, help="time to wait between dumps of the virtual machines memory (5 seconds it built into this)", 
                  metavar="TIMEBETWEENDUMPS")


parser.add_argument("-d", "--dst_dir", type=str,
                  default=None, help="destination directory on the remote host", 
                  metavar="DST_DIR")

parser.add_argument("-p", "--dst_port", type=int,
                  default=None, help="",
                  metavar="DST_PORT")

parser.add_argument("-o", "--dst_host", type=str,
                  default=None, help="remote host to copy files too", 
                  metavar="DST_HOST")

parser.add_argument("-u", "--user", type=str,
                  default=None, help="username for the remote host", 
                  metavar="USER")

parser.add_argument("-a", "--password", type=str,
                  default=None, help="password for the user account", 
                  metavar="DST_HOST")
                  
                  
    
if __name__ == '__main__':
    
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)
    logging.debug("Using vbox location: %s"%(args.vboxlocation))
    logging.debug("Using cuckoo location: %s"%(args.cuckoolocation))
    logging.debug("Using machine %s for analysis"%(args.machine))
    logging.debug("Dumpig memory to: %s"%(args.savelocation))
    logging.debug("Running sandbox for %s seconds"%(str(args.runtime)))
    logging.debug("Dumping memory every  %s seconds"%(str(args.timing+5)))
    logging.debug("Waiting %s seconds before dumps start"%(str(args.initwait)))

    
    main( args.cuckoolocation, args.machine, args.malware_file, 
          args.savelocation, args.runtime, args.timing, args.initwait, 
          args.vboxlocation, host=args.dst_host, port=args.dst_port, 
          user=args.user, password=args.password, dst_dir=args.dst_dir)
    
    



    
    
                  
                  
