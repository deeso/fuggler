__author__ = 'apridgen'
import datetime, os, pymongo
import threading, subprocess, time, json
from pymongo import Connection


import volatility.conf as conf
import volatility.registry as registry
import volatility.commands as commands
import volatility.addrspace as addrspace
import argparse

DEFAULT_CMDS = ['printkey', 'apihooks', 'callbacks', 'cmdscan', 'devicetree', 'connscan',
                'consoles', 'dlllist', 'driverirp', 'driverscan', 'envars', 'filescan',
                'getsids', 'gdt', 'handles',  'idt', 'memmap', 'ssdt', 'mutantscan',
                'sockscan', 'sockets',  'threads', 'pslist', 'psscan', 'timers', 'vadtree']


BASE_CONF = {'profile': '',
             'use_old_as': None,
             'kdbg': None,
             'help': False,
             'kpcr': None,
             'tz': None,
             'pid': None,
             'output_file': None,
             'physical_offset': None,
             'conf_file': None,
             'dtb': None,
             'output': None,
             'info': None,
             'location': '',
             'plugins': None,
             'debug': None,
             'cache_dtb': True,
             'filename': None,
             'cache_directory': None,
             'verbose': None,
             'write':False,
             'regexp':None,
             }

get_timestamp = lambda x: int(os.path.split(x)[-1].split('_')[0])

class VolatilityExtractSubprocess(object):
    the_file_fmt = "file://%s"
    def __init__(self, image_dir, db_name, host='127.0.0.1', port=27017,
                 user="", password="", profile='WinXPSP3x86', max_threads=10,
                 cmds = DEFAULT_CMDS, init_db = True,
                 image_name = None):

        self.image_dir = image_dir
        self.db_name = db_name
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.profile = profile
        self.max_threads = max_threads

        self.cmds = cmds



        self.image_name = image_name
        if image_name is None:
            self.image_name = self.db_name

        self.files = self.get_files(image_dir)

        self.conn, self.db = self.get_connection()

        self.running_threads = []

        if init_db:
            self.init_db()

    def get_files(self, image_dir):
        files = [os.path.join(image_dir, i) for i in os.listdir(image_dir)]
        files.sort()
        return files

    def get_connection(self):
        connection = Connection(self.host, self.port, journal=True, safe=True)
        return (connection, connection[self.db_name])


    def create_collections(self):
        print ("Creating the collections and indexes for all %d commands"%len(self.cmds))
        for cmd in self.cmds:
            col = self.db[cmd]
            col.create_index([('timestamp', pymongo.ASCENDING)], cache_for=300)
        print ("Done creating the collections and indexes for all %d commands"%len(self.cmds))

    def init_db(self):

        registry.PluginImporter()
        config = conf.ConfObject()
        registry.register_global_options(config, commands.Command)
        registry.register_global_options(config, addrspace.BaseAddressSpace)
        cmds = registry.get_plugin_classes(commands.Command, lower = True)
        f = self.files[10]

        # default config

        for k,v in BASE_CONF.items():
            config.update(k, v)

        config.update("profile", self.profile)
        config.update('location', self.the_file_fmt%f)
        cmd_instance = cmds['imageinfo'](config)


        imageinfo = {'ImageName':self.image_name,
                     'ImageInfo':cmd_instance.get_json(cmd_instance.calculate()),
                     'TimeStart':get_timestamp(self.files[0]),
                     'TimeEnd':get_timestamp(self.files[-1]),
                     'ImageCount':len(self.files)
        }

        self.db.metadata.insert(imageinfo)

        self.create_collections()


    def run_block(self):
        cnt = 1
        start_time = datetime.datetime.now()

        try:
            for f in self.files:
                file_start_time = datetime.datetime.now()
                timestamp = get_timestamp(f)
                print ('Started processing file, (%d) %s'%(cnt, f))
                for cmd in self.cmds:

                    while self.max_threads < len(self.running_threads):
                        #print ("looks like more threads than allowed are running: %d"%(len(running_threads)))
                        self.running_threads = self.prune_threads()
                        #print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
                        if self.max_threads > len(self.running_threads):
                            break
                        time.sleep(1)

                    t = threading.Thread(target=self.execute_command, args=(timestamp, f, cmd ))
                    t.start()
                    self.running_threads.append(t)


                file_end_time = datetime.datetime.now()
                print ('Processing file,(%d) %s, took %d seconds'%(cnt, f, (file_end_time - file_start_time).seconds))
                cnt += 1

        except KeyboardInterrupt:
            pass

        while len(running_threads) > 1:
            print ("waiting on %d threads"%(len(running_threads)))
            running_threads = self.prune_threads(running_threads)
            print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
            if 1 > len(running_threads):
                break
            time.sleep(1)
        end_time = datetime.datetime.now()
        print ("Completed processing all files in %d seconds."%(end_time - start_time).seconds)

    def prune_threads(self):
        return [t for t in self.running_threads if t.is_alive()]


    def execute_command(self, timestamp, filename, vol_cmd):
        vol_fmt = "/usr/local/bin/vol.py --output json --profile %s -f %s %s"
        cmd_str = vol_fmt%(self.profile, filename, vol_cmd)
        print ("Executing command (%s) on file: %s"%(vol_cmd, filename))
        #print ("executing command %s"%str(cmd_str.split()))
        p = subprocess.Popen(cmd_str.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(.5)
        out = ''
        # need to read the data in this manner, because the
        # process will not complete writing the buffer and
        # all the threads will dead lock
        while True:
            try:
                pipes = p.communicate()
                out = out + pipes[0]
            except:
                break


        results = {}
        try:
            results = json.loads(out)
            print ("**** Completed to execute command (%s) on file: %s"%(vol_cmd, filename))
        except:
            print ("XXXX Failed to execute command (%s) on file: %s"%(vol_cmd, filename))
            #raise


        #conn, db = self.get_connection()


        results['timestamp'] = timestamp
        results['image_name'] = self.db_name
        coll = self.db[vol_cmd]
        coll.insert(results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()


    parser.add_option("-n", "--netport", type=int,
        default=27017, dest="mongo_port",
        help="mongo_port to connect too", metavar="PORT")

    parser.add_argument("-s", "--server", type=str,
        default='127.0.0.1', dest="mongo_host",
        help="mongo_host to connect too", metavar="HOST")


    parser.add_option("-d", "--db_name", dest="db_name", type=str,
        default=None, help="database name to store output in",
        metavar="DB_NAME")

    parser.add_option("-i", "--input_dir", dest="input_dir", type=str,
        default=None, help="input directory with images",
        metavar="IMAGES_DIR")

    parser.add_option("-r", "--profile", dest="profile", type="string",
        default='WinXPSP3x86', help="volatile profile of memory images",
        metavar="IMAGE_PROFILE")

    parser.add_option("-u", "--user", default='', dest="mongo_user",
        help="user name for mongo", metavar="USER")

    parser.add_option("-p", "--password", default='', dest="mongo_password",
        help="password for mongo", metavar="PASSWORD")

    parser.add_option("-t", "--threads", default=10, dest="threads", type="int",
        help="number of threads to run concurrently", metavar="THREADS")

    args = parser.parse_args()
    ve = VolatilityExtractSubprocess(args.input_dir, args.db_name,
        host=args.host, port=args.port, profile="WinXPSP3x86",
        user=None, password=None, max_threads=38)

    ve.run_block()
