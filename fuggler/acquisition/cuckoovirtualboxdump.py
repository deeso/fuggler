__author__ = 'apridgen'

import sys, logging, subprocess, time, os, datetime

# for the time being I sourced this from 
#http://stackoverflow.com/questions/273192/python-best-way-to-create-directory-if-it-doesnt-exist-for-file-write
import os
import errno

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

class CuckooVboxDump(object):

    def __init__(self, malware_file, cuckoo_location, machine, savelocation,
                 runtime, sleep_time, init_wait, vbox_location, dump_callback = None,
                 allow_load_time = 5, package='exe', priority=99):

        self.package=package
        self.priority=priority
        self.malware_filename = malware_file
        self.cuckoo_location = cuckoo_location
        self.machine = machine
        self.savelocation = savelocation
        self.runtime = runtime
        self.sleep_time = sleep_time
        self.init_wait = init_wait

        self.dump_callback = dump_callback
        self.allow_load_time = allow_load_time
        self.current_time = 0
        self.vbox_location = vbox_location
        self.max_time_to_run = self.runtime + (self.runtime/self.sleep_time) *\
                                             allow_load_time + init_wait
        self.SNAPSHOTS_COMPLETE = False
        # fail if the path to the output does not exist
        make_sure_path_exists(self.savelocation)
        self.END = False

    def get_ticks(self):
        return self.runtime

    def processing_complete(self):
        return  self.SNAPSHOTS_COMPLETE

    def stop(self):
        self.END = True

    def start(self, shutdown_machine=False):
        self.END = False
        self.submit_malware()
        self.current_time = 0
        start_time = datetime.datetime.now()
        
        while not self.END:
            self.current_time = (datetime.datetime.now() - start_time).seconds
            if self.current_time > self.max_time_to_run:
                break

            logging.debug("Ticks: %04i Saving state with debugvm"%self.current_time)
            sav = "%04i_%s_img.bin"%(self.current_time, self.machine)

            img_filename = os.path.join(self.savelocation, sav)
            print img_filename
            p = subprocess.Popen([self.vbox_location, "debugvm", self.machine,
                              "dumpguestcore", "--filename", img_filename],

            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print p.communicate()
            p.wait()

            if not self.dump_callback is None:
                self.dump_callback(sav)

            time.sleep(self.sleep_time + self.allow_load_time)

        self.SNAPSHOTS_COMPLETE = True
        if shutdown_machine:
            p = subprocess.Popen([self.vbox_location, "controlvm", self.machine, "poweroff"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            p.wait()


    def update_for_cuckoo(self):
        sys.path.append(self.cuckoo_location)
        from lib.cuckoo.common.utils import File
        from lib.cuckoo.core.database import Database


    def submit_malware(self):
        self.update_for_cuckoo()
        from lib.cuckoo.common.utils import File
        from lib.cuckoo.core.database import Database

        db = Database()
        task_id = db.add(file_path=self.malware_filename,
            md5=File(self.malware_filename).get_md5(),
            package=self.package,
            priority=self.priority)

        logging.debug('Started analysis task: %d'%task_id)
        return task_id



if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("malware_file", type=str,
        help="/path/to/the/malware.file ", metavar="PATH_TO_MALWARE")

    parser.add_argument("machine", type=str,
        help="machine to submit the malware too", metavar="MACHINE")

    parser.add_argument("cuckoolocation", type=str,
        help="location of cuckoo", metavar="CUCKOO")

    parser.add_argument("savelocation", type=str,
        help="location where to save memory images",
        metavar="SAVELOCATION")


    parser.add_argument("-v", "--vboxlocation", type=str,
        default='/usr/bin/vboxmanage', help="/path/to/vboxmanage is located",
        metavar="VBOXLOCATION")


    parser.add_argument("-r", "--runtime", type=int,
        default=3600, help="number of seconds to run for (3600 == 1 hour)",
        metavar="RUNTIME")

    parser.add_argument("-i", "--initwait", type=int,
        default=30, help="time to wait before starting the dump of the virtual machines memory",
        metavar="INITTIME")

    parser.add_argument("-t", "--timing", dest="sleep_time", type=int,
        default=30, help="time to wait between dumps of the virtual machines memory (5 seconds it built into this)",
        metavar="TIMEBETWEENDUMPS")


    parser.add_argument("-d", "--dst_dir", type=str,
        default=None, help="destination directory on the remote host",
        metavar="DST_DIR")

    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)
    logging.debug("Using vbox location: %s"%(args.vboxlocation))
    logging.debug("Using cuckoo location: %s"%(args.cuckoolocation))
    logging.debug("Using machine %s for analysis"%(args.machine))
    logging.debug("Dumpig memory to: %s"%(args.savelocation))
    logging.debug("Running sandbox for %s seconds"%(str(args.runtime)))
    logging.debug("Dumping memory every  %s seconds"%(str(args.timing+5)))
    logging.debug("Waiting %s seconds before dumps start"%(str(args.initwait)))


    cvbox_snapshots = CuckooVboxDump( args.malware_file, args.cuckoolocation, args.machine,
        args.savelocation, args.runtime, args.sleep_time, args.initwait,
        args.vboxlocation)

    cvbox_snapshots.start()

    while not cvbox_snapshots.processing_complete():
        logging.debug("Polling: %s still running"%datetime.datetime.now())

    logging.debug("Processing complete: %s"%datetime.datetime.now())
