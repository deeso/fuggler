__author__ = 'apridgen'

import datetime, traceback, os, pymongo, sys, argparse
import threading, subprocess, time, json, logging
import paramiko

class SynchronizedQueue(list):
    def __init__(self):
        super(SynchronizedQueue, self).__init__(self)
        self.lock = threading.Lock()

    def append(self, item):
        try:
            self.lock.acquire()
            super(SynchronizedQueue, self).append(item)
        except:
            pass
        finally:
            self.lock.release()

    def insert(self, item):
        try:
            self.lock.acquire()
            super(SynchronizedQueue, self).insert(item)
        except:
            pass
        finally:
            self.lock.release()


    def push(self, item):
        self.append(item)

    def pop(self, idx=0):
        x = None
        try:
            self.lock.acquire()
            x = super(SynchronizedQueue, self).pop(0)
        except:
            pass
        finally:
            self.lock.release()

        return x

    def insert0(self, item):
        try:
            self.lock.acquire()
            super(SynchronizedQueue, self).insert(item, 0)
        except:
            pass
        finally:
            self.lock.release()

    def top(self):
        if len(self) > 0:
            return self[-1]
        return None

class SFTPSnapshots(object):

    def __init__(self, user, password, local_base_path="",
                    host="127.0.0.1", port=22,  dst_base_path="",
                    max_threads = 10):

        if user is None or password is None:
            raise "User or password must be specified!"

        self.user = user
        self.password = password
        self.local_base_path = local_base_path
        self.host = host
        self.port = port
        self.dst_base_path = dst_base_path
        self.queue = SynchronizedQueue()
        self.threads = SynchronizedQueue()
        self.max_threads = max_threads

        self.END = True
        self.END_GRACEFUL = True
        self.consumer = threading.Thread(target=self.run)
        self.consumer.start()


    def execute_move(self, filename):

        full_local_file = os.path.join(self.local_base_path, filename)
        full_dst_file =  os.path.join(self.dst_base_path, filename)
        dst_filepath = os.path.split(full_dst_file)[0]
        fname = os.path.split(full_local_file)[1]

        transport = paramiko.Transport((self.host, self.port))
        logging.debug("Connected to %s:%d"%(self.host, self.port))

        transport.connect(username = self.user, password = self.password)

        sftp = paramiko.SFTPClient.from_transport(transport)

        self.sftp_recursively_create_dirs(sftp, dst_filepath)

        sftp.put(full_local_file, full_dst_file)
        logging.debug("File xfr completed for: %s"%(fname))
        sftp.close()
        transport.close()


    def sftp_recursively_create_dirs(self, sftp, path):
        dir_to_add = os.path.split(path)

        try:
            sftp.stat(path)
            return
        except:
            pass

        # trailiing slash
        if len(dir_to_add[0]) > 1 and len(dir_to_add[1]) <= 1:
            self.sftp_recursively_create_dirs(sftp, dir_to_add[0])
            return
        # at the root return
        elif len(dir_to_add[0]) <= 1:
            return

        try:
            sftp.stat(dir_to_add[0])
        except:
            self.sftp_recursively_create_dirs(sftp, dir_to_add[0])

        sftp.mkdir(path)

    def add_file(self, filename):
        self.queue.append(filename)

    def consume_one(self):
        if len(self.queue) == 0:
            return
        filename = self.queue.pop(0)
        logging.debug("Transfering %s to (%s, %d)"%(filename, self.host, self.port))
        t = threading.Thread(target=self.execute_move, args=(filename,))
        t.start()
        self.threads.append(t)

    def pending_transfers(self):
        return len(self.queue) + len(self.threads)

    def prune_threads(self):
        t = SynchronizedQueue()
        for i in self.threads:
            if i.is_alive():
                t.append(i)
        self.threads = t

    def stop(self, graceful = True):
        self.END = True
        self.END_GRACEFUL = graceful

    def run(self):
        self.END = False

        try:
            while  not self.END:
                if len(self.threads) < self.max_threads:
                    self.consume_one()

                if len(self.threads) > 0:
                    self.prune_threads()
                time.sleep(1)
        except:
            pass

        if not self.END_GRACEFUL:
            try:
                while len(self.threads) > 0:
                    if len(self.threads) > 0:
                        self.prune_threads()
                    time.sleep(.5)
            except:
               # thread killa
               for i in self.threads:
                   del i
               raise 





if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", "--local_dir", type=str,
        default="", help="base destination directory on the remote host",
        metavar="LDST_DIR")

    parser.add_argument("-r", "--remote_dir", type=str,
        default="", help="base destination directory on the remote host",
        metavar="RDST_DIR")

    parser.add_argument("-n", "--port", type=int,
        default=22, help="sftp destination port",  metavar="DST_PORT")

    parser.add_argument("-o", "--host", type=str,
        default="127.0.0.1", help="remote host to copy files too",
        metavar="DST_HOST")

    parser.add_argument("-u", "--user", type=str,
        default=None, help="username for the remote host",
        metavar="USER")

    parser.add_argument("-p", "--password", type=str,
        default=None, help="password for the user account",
        metavar="PASSWORD")

    parser.add_argument("-t", "--max_threads", type=str,
        default=10, help="max number of threads to use",
        metavar="MAX_THREADS")

    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)

    sftp = SFTPSnapshots( args.user, args.password,
        local_base_path=args.local_dir,
        host=args.host, port=args.port,
        dst_base_path=args.remote_dir,
        max_threads = args.max_threads)

    files = os.listdir(args.local_dir)
    for i in files:
        sftp.add_file(i)

    while sftp.pending_transfers() > 0:
        print "Waiting on %d transfers"%(sftp.pending_transfers())


    print ("Done transfering files")
