__author__ = 'apridgen'




# remember
#ssh -L 27018:localhost:27017 dso@dsostar.internal.thecoverofnight.com -p 443

from pymongo import Connection

host = '127.0.0.1'
port = 27018


con = Connection(host, port)

pid_to_compare = ['baseline', 'cridex']


def find_all_new_pids(db):
    p = [i for i in db['pslist'].find()]
    pid_ts = {entry['timestamp']:get_pid_image(entry) for entry in p}
    pid_ts_keys = pid_ts.keys()
    pid_ts_keys.sort()

    new_ts = {ts:set() for ts in pid_ts_keys}
    seen = set()
    last_new = set()
    new = set()
    done = set()
    done_new = set()

    for ts in pid_ts_keys:
        #print pid_ts[ts]
        for pid, image, cmdline in pid_ts[ts]:
            tag = str([pid, cmdline])
            if not tag in seen:
                new_ts[ts].add(tag)
                seen.add(tag)

        for tag in new_ts[ts]:
            pid, cmdline = eval(tag)
            print "At %s: pid (%s) started w/ cmdline: %s"%(ts, pid, cmdline)
    return new_ts




def compare_against_baseline(conn, db_name):
    base_line_ps = find_all_new_pids(conn['base'])

    for name in pid_to_compare:
        p = find_all_new_pids(con[name])
        process_lists.append(p)



def find_file_name(db, FileName, ts = None):
    filescan_results = None
    if ts:
        filescan_results = [i for i in db['filescan'].find() if i['timestamp'] == ts]
    else:
        filescan_results = [i for i in db['filescan'].find()]

    for r in filescan_results:
