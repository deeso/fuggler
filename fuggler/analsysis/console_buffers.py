__author__ = 'apridgen'


# remember
#ssh -L 27018:localhost:27017 dso@dsostar.internal.thecoverofnight.com -p 443

from pymongo import Connection

host = '127.0.0.1'
port = 27018


con = Connection(host, port)

db = con['winxpsp3_pics']


p = [i for i in db['pslist'].find()]
get_pid_image = lambda entry: [ (i, entry[i][u'ImageFileName'], entry[i][u'CommandLine']) for i in entry if i.isdigit()]
pid_ts = {entry['timestamp']:get_pid_image(entry) for entry in p}

def find_all_new_pids(db):
    p = [i for i in db['pslist'].find()]
    pid_ts = {entry['timestamp']:get_pid_image(entry) for entry in p}
    pid_ts_keys = pid_ts.keys()
    pid_ts_keys.sort()

    new_ts = {ts:set() for ts in pid_ts_keys}
    seen = set()
    last_new = set()
    new = set()
    done = set()
    done_new = set()

    for ts in pid_ts_keys:
        #print pid_ts[ts]
        for pid, image, cmdline in pid_ts[ts]:
            tag = str([pid, cmdline])
            if not tag in seen:
                new_ts[ts].add(tag)
                seen.add(tag)

        for tag in new_ts[ts]:
            pid, cmdline = eval(tag)
            print "At %s: pid (%s) started w/ cmdline: %s"%(ts, pid, cmdline)
    return new_ts

def collect_all_buffers(console_collection):

    buffers_seen = set()
    snapshots = {}
    time_series = set()
    console_list = [i for i in console_collection.find() ]

    for console in console_list:
        offsets = [i for i in console.keys() if i.isdigit()]
        for offset in offsets:
            app = console[offset]
            if not u'Screen' in app:
                continue
            for screen in app[u'Screen']:
                if len(screen[u'Buffer']) > 0:
                    time_series.add(console[u'timestamp'])

                if len(screen[u'Buffer']) > 0 and\
                   not screen[u'Buffer'] in buffers_seen:
                    if not console[u'timestamp'] in snapshots:
                        snapshots[console[u'timestamp']] = []
                    snapshots[console[u'timestamp']].append({
                        u'ImageFileName':console[offset][u'ImageFileName'],
                        u'Buffer':screen[u'Buffer']})
                    buffers_seen.add(screen[u'Buffer'])


    return snapshots, time_series


consoles_col = db['consoles']

cur = consoles_col.find()
sn, ts = collect_all_buffers(consoles_col)

r = [i for i in ts]
r.sort()
#print r

for key in sn:
    for scr in sn[key]:
        print key, scr[u'Buffer']


connections = db['connscan'].find({'timestamp':r[0]})

targethost = u'46.23.75.120'

i = connections.next()
k = [i[j] for j in i if j.isdigit()]

#print [j for j in k if 'RemoteIpAddress' in j and j['RemoteIpAddress'] == targethost]


p = [i for i in db['pslist'].find()]
get_pid_image = lambda entry: [ (i, entry[i][u'ImageFileName']) for i in entry if i.isdigit()]
pid_ts = {entry['timestamp']:get_pid_image(entry) for entry in p}

find_all_new_pids(db)