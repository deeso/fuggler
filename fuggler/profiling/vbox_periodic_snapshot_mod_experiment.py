import os
import time
import logging
import subprocess
import os.path
import datetime
import threading 
import time

i = 0
time_to_run = 4600
sleep_time = 10
allow_load_time = 5
max_time_to_run = time_to_run + (time_to_run/sleep_time) * allow_load_time


#malware_file = "/sandbox/malware/shamoon/D214C717A357FE3A455610B197C390AA.exe"
#fmt_cmd_str = "python /sandbox/code/git/cuckoo/utils/submit.py --timeout %i --machine %s %s"

# machine is the same as VM name
machine = "winxpsp3_analysis"

#cmd_str = fmt_cmd_str%(max_time_to_run, machine, malware_file )
#p = subprocess.Popen(cmd_str.split(),
#                     stdout=subprocess.PIPE,
#                     stderr=subprocess.PIPE)
#p.wait()



# place where saved state ends up
dst_snapshot = "/research_data/experiment_snapshot/"




start_time = datetime.datetime.now()
run_time = 0
while True:
    run_time = (datetime.datetime.now() - start_time).seconds
    if run_time > max_time_to_run:
        break
        
    print "Ticks: %04i Saving state with debugvm"%run_time
    sav = "%04i_%s_img.bin"%(run_time, machine)
    
    img_filename = os.path.join(dst_snapshot, sav)
    p = subprocess.Popen(["/usr/bin/vboxmanage", "debugvm", machine, "dumpguestcore", "--filename", img_filename],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    p.wait()
    
    print "Decrement timer"
    time.sleep(sleep_time + allow_load_time)
    
    
run_time = datetime.datetime.now() - start_time
print "Stopped snapping shots after %i seconds"%run_time
