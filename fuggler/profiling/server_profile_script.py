__author__ = 'apridgen'


import datetime, time, socket


current = datetime.datetime.now()
last = current;

# related to run times
max_run_time = 5200
run_time = 0


# instrumentation code
#logfile = open("C:\\timer_logs.txt", 'w')
logfile = open("//home//dso//timer_logs.txt", 'w')
accumulator = 0
window = []
max_window_sz = 512
max_window_sz_bshift = 9
power_shift = {2:1,4:2,8:3,16:4,32:5,64:6,128:7,256:8,512:9,1024:10}


# network destination code
host = "10.16.52.19"
port = 30809

ssock = socket.socket()
ssock.bind((host, port))
ssock.listen(1)


def update_time(last):
    global logfile, window, max_window_sz, ssock, accumulator
    current = datetime.datetime.now()
    ctime = (current - last).microseconds
    window.append(ctime)

    accumulator = (accumulator + ctime) >> 1
    if len(window) > max_window_sz:
        window.pop(0)

    output_str = ''
    if len(window) in power_shift:
        output_str = "%f %d, %d, %d\n"%(time.mktime(last.timetuple()), ctime, accumulator, sum(window) >> power_shift[len(window)])
    else:
        output_str = "%f %d, %d, %d\n"%(time.mktime(last.timetuple()), ctime, accumulator, sum(window) / len(window))

    print output_str
    logfile.write(output_str)
    return datetime.datetime.now()


client, a = ssock.accept()
data = client.recv(1)
print "waiting for client connections"
last = datetime.datetime.now()
current = datetime.datetime.now()



print "running code"
while run_time < max_run_time:


    last = current
    tlast = datetime.datetime.now()
    current = update_time(current)
    run_time += ((current - last) + (tlast - last)).seconds









