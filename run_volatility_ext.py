import argparse
from  fuggler.acquisition.volatilityextract import VolatilityExtractSubprocess

parser = argparse.ArgumentParser()


parser.add_argument("-n", "--port", type=int,
    default=27017, dest="port",
    help="mongo_port to connect too", metavar="PORT")

parser.add_argument("-s", "--server", type=str,
    default='127.0.0.1', dest="server",
    help="mongo_host to connect too", metavar="HOST")


parser.add_argument("-d", "--db_name", dest="db_name", type=str,
    default=None, help="database name to store output in",
    metavar="DB_NAME")

parser.add_argument("-i", "--input_dir", dest="input_dir", type=str,
    default=None, help="input directory with images",
    metavar="IMAGES_DIR")

parser.add_argument("-r", "--profile", dest="profile", type=str,
    default='WinXPSP3x86', help="volatile profile of memory images",
    metavar="IMAGE_PROFILE")

parser.add_argument("-u", "--user", default='', dest="mongo_user", type=str,
    help="user name for mongo", metavar="USER")

parser.add_argument("-p", "--password", default='', dest="mongo_password", type=str,
    help="password for mongo", metavar="PASSWORD")

parser.add_argument("-t", "--threads", default=10, dest="threads", type=int,
    help="number of threads to run concurrently", metavar="THREADS")

args = parser.parse_args()
if args.input_dir is None:
   parser.print_help()
   raise

ve = VolatilityExtractSubprocess(args.input_dir, args.db_name,
    host=args.server, port=args.port, profile="WinXPSP3x86",
    user=None, password=None, max_threads=38)

ve.run_block()
